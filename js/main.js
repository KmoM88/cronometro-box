let mil = 00;
let sec = 00;
let min = 3;
let appendMil = document.getElementById("mil");
let appendSec = document.getElementById("sec");
let appendMin = document.getElementById("min");
let buttonStart = document.getElementById("btnStart");
let buttonStop = document.getElementById("btnStop");
let buttonReset = document.getElementById("btnReset");
let sound1 = document.getElementById("beep1");
let sound2 = document.getElementById("beep2");
let interval;
let estado = 0;
const zeroPad = (num, places) => String(num).padStart(places, "0");

function startTimer() {
  mil--;

  if (mil < 0) {
    mil = "99";
    sec--;
    appendMil.innerHTML = zeroPad(mil, 2);
    appendSec.innerHTML = zeroPad(sec, 2);
  }
  if (mil >= 0) {
    appendMil.innerHTML = zeroPad(mil, 2);
  }

  if (sec < 0) {
    sec = "59";
    min--;
    appendSec.innerHTML = zeroPad(sec, 2);
    appendMin.innerHTML = zeroPad(min, 1);
  }

  if (min < 0) {
    sound2.play();
    estado = 0;
    clearInterval(interval);
    mil = "00";
    sec = "00";
    min = "3";
    appendMin.innerHTML = min;
    appendSec.innerHTML = sec;
    appendMil.innerHTML = mil;
  }
}

buttonStart.onclick = function () {
  if (estado == 0) {
    sound1.play();
    estado = 1;
    interval = setInterval(startTimer, 10);
  }
};

buttonStop.onclick = function () {
  sound2.play();
  estado = 0;
  clearInterval(interval);
};

buttonReset.onclick = function () {
  sound2.play();
  estado = 0;
  clearInterval(interval);
  mil = "00";
  sec = "00";
  min = "3";
  appendMin.innerHTML = min;
  appendSec.innerHTML = sec;
  appendMil.innerHTML = mil;
};
